package simulare;

public class Timer {
	private long startTime;
	//se salveaza timpul la care incepe simularea
	public void start() {
		startTime = System.currentTimeMillis();
		//e o metoda care returneaza timpul curent in milisec, e declarat cu long..
		
	}
	public long getSecondsPassed() {
		return (System.currentTimeMillis()-startTime)/1000;
		//calculeaza nr de secunde de la inceperea simularii(tranform milisecundele in secunde, imapartind la 1000)
	}
	

}
