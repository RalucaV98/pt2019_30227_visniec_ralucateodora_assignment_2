package simulare;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import casaDeMarcat.CashRegister;
import javafx.util.Pair;
import model.Client;

public class SimulationHandler implements Runnable {

    private Timer timer;
    private List<CashRegister> cashRegisters;
    private long finalTime;
    private int totalCustomers = 0;
    private long minServiceTime;
    private long maxServiceTime;
    private long minArrivingTime;
    private long maxArrivingTime;


    private long totalWaitingTime = 0;
    private long totalClientsServiceTime = 0;

    public SimulationHandler(Timer timer, List<CashRegister> cashRegisters,
                             long finalTime, long minServiceTime,
                             long maxServiceTime, long minArrivingTime, long maxArrivingTime) {
        this.timer = timer;
        this.cashRegisters = cashRegisters;
        this.finalTime = finalTime;
        this.minServiceTime = minServiceTime;
        this.maxServiceTime = maxServiceTime;
        this.maxArrivingTime = maxArrivingTime;
        this.minArrivingTime = minArrivingTime;
    }

    /**
     * Genereaza clienti la un interval de (minArrivingTime, maxArrivingTime)
     * Dupa generarea clientului acesta este pus in sleep
     * Acest client se introduce in coada casei care are cel mai putin timp de asteptat
     * Se scrie in fisier folosindu-ma de clasa Logger: casa, timpul la care a ajuns si client-ul
     */
    public void run() {
        int randomSleep;
        Random random = new Random();
        while (timer.getSecondsPassed() < finalTime) {
            randomSleep = (int) (random.nextInt((int) this.maxArrivingTime) + this.minArrivingTime); // intervalul la care ajunge un client la casa
            try {
                Thread.sleep(randomSleep * 1000);
                totalCustomers++;
                Client client = new Client(
                        totalCustomers,
                        timer.getSecondsPassed(),
                        random.nextInt((int) this.maxServiceTime) + this.minServiceTime
                );
                totalClientsServiceTime += client.getServiceTime();
                int cashRegisterSelected = -1;
                int minValue = 100;
                for (int i = 0; i < cashRegisters.size(); i++) { // selectez casa unde timpul de asteptare e cel mai mic
                    if (cashRegisters.get(i).getTotalWaitingTime() < minValue) {
                        minValue = (int) cashRegisters.get(i).getTotalWaitingTime();
                        cashRegisterSelected = i;
                    }
                }

                this.totalWaitingTime += minValue;

                this.cashRegisters.get(cashRegisterSelected).addClient(client);
                Logger logger = Logger.getInstance();
                String message = "Coada " + cashRegisterSelected + ":a ajuns la secunda " + timer.getSecondsPassed() + " " + client;
                logger.write(message);
            } catch (InterruptedException | IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * Returneaza numarul total de customeri generati in aplicatie
     * @return
     */
    public int getTotalCustomers() {
        return this.totalCustomers;
    }

    /**
     * Returneaza timpul mediu de asteptare al clientilor
     * @return
     */
    public float getAverageWaitingTime() {
        return this.totalWaitingTime / (float) this.totalCustomers;
    }

    /**
     * Returneaza timpul mediu de servire a clientilor
     * @return
     */
    public float getAverageOfServingTime() {
        return this.totalClientsServiceTime / (float) this.totalCustomers;
    }

    /**
     * Calculeaza ora de varf a simularii la care au fost cei mai multi clienti
     * Key -> ora
     * Value -> numarul de clienti la ora respectiva
     * @return
     */
    public Pair<Integer, Integer> getPeekHour() {
        int peekHour = 0;
        int maxHourValue = 0;
//fiecare ora de pe durata simularii
        for (int h = 0; h < cashRegisters.get(0).getCashRegisterEvolution().size(); h++) {
            int hourValue = 0;
            for (int c = 0; c < cashRegisters.size(); c++) {
                List<Integer> hoursEvolution = cashRegisters.get(c).getCashRegisterEvolution();
                int hour = 0;

                if (hoursEvolution.size() > h + 1) {
                    hour = hoursEvolution.get(h);
                }
                // ii pus numarul total de clienti de la fiecare casa insumate la h respectiv
                hourValue += hour;
            }

            if (hourValue > maxHourValue) {
                peekHour = h;
                maxHourValue = hourValue;
            }
        }

        return new Pair<>(peekHour + 1, maxHourValue);
    }

    /**
     * Returneaza datele necesare pentru construirea tabelui de evolutie a caselor in functie de ora
     * Key -> datele
     * Value -> coloanele
     * @return
     */
    public Pair<Object[][], Object[]> getSimulationEvolution() {
        Object[] columns = new Object[this.cashRegisters.size() + 1];
        columns[0] = "Seconds";
        int maxHourLenght = - 1;
        for (int i = 0 ; i < cashRegisters.size(); i++) {
            columns[i+1] = "Cash register " + cashRegisters.get(i).getId();
            if (maxHourLenght < cashRegisters.get(i).getCashRegisterEvolution().size()) {
                maxHourLenght = cashRegisters.get(i).getCashRegisterEvolution().size();
            }
        }

        Object[][] data = new Object[maxHourLenght+1][this.cashRegisters.size() + 1];
        for (int h = 0; h < maxHourLenght; h++) {
            data[h][0] = "Second " + (h+1);

            for (int c = 0 ; c < this.cashRegisters.size(); c++) {
                List<Integer> hoursEvolution = cashRegisters.get(c).getCashRegisterEvolution();
                int hour = (hoursEvolution.size() > h) ? hoursEvolution.get(h) : 0;
                data[h][c+1] = hour;
            }
        }

        return new Pair<>(data, columns);
    }
}
