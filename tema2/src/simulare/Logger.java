package simulare;

import java.io.FileWriter;
import java.io.IOException;

public class Logger {
   //toate loggurile vor pointa spre acelasi obiect
	private static Logger instance; //SingleTon design pattern class have its constructor as private and have a static instance of itself.
	private FileWriter fileWriter;
	
	//Singleton design pattern
	//throws IOException
	
	private Logger() throws IOException {
		fileWriter = new FileWriter("simulation.log");
	}

	// pentru a avea un singur obiect pe durata programului.
	public static Logger getInstance() throws IOException {
		if(instance==null) {
			instance=new Logger();
		}
		return instance;
	}
	
	//scriem detaliile simularii in log(fisier) + consola
	
	public synchronized void write (String message) throws IOException{
		System.out.println(message);
		this.fileWriter.write(message + "\n");
	}
	
	//inchidem fisierul de log pe care l-am deschis
	
	public void closeFile() throws IOException {
		this.fileWriter.close();
	}
	
}
