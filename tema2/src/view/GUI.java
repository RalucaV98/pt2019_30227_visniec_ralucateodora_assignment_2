package view;

import casaDeMarcat.CashRegister;
import javafx.util.Pair;
import model.Client;
import simulare.Logger;
import simulare.SimulationHandler;
import simulare.Timer;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class GUI  {
    private JFrame frame = new JFrame("Simulare Cozi");
	private JPanel panou;
	
	private JLabel minServTime;
    private JTextField txtFieldMinService = new JTextField("1");
    
    private JLabel maxServTime;
    private JTextField txtFieldMaxService = new JTextField("2");
    
    private JLabel minArv;
    private JTextField txtFieldMinArriving = new JTextField("1");
    
    private JLabel maxArv;
    private JTextField txtFieldMaxArriving = new JTextField("3");
    
    private JLabel totalTime;
    private JTextField txtFieldTotalTime = new JTextField("20");
    
    private JLabel cashRegCount;
    private JTextField txtFieldCashRegisterCount = new JTextField("3");
    
    private JLabel start;
    private JButton startBtn = new JButton("Start simulation");

    Color LightSalmon = new Color(255, 160, 122); //simulare
    Color LightPink = new Color(255, 182, 193);//background

    private Timer timer;
    private List<CashRegister> cashRegisterList = new ArrayList<>();
    private SimulationHandler simulationHandler = null;
    private SimulationPanel simulationPanel;
    public GUI() {
    	super();
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setSize(700,700);
     
        panou=new JPanel();
        panou.setBackground(LightPink);
        panou.setLayout(new BoxLayout(panou, BoxLayout.Y_AXIS));
        panou.setBounds(0, 0, 600, 600);
        txtFieldMinArriving.setSize(30, 30);
        txtFieldMaxArriving.setSize(30, 30);
        txtFieldMinService.setSize(30,30);
        txtFieldMaxService.setSize(30, 30);
        txtFieldCashRegisterCount.setSize(30, 30);

        
        minServTime=new JLabel ("Min service time");
        panou.add(minServTime);
        panou.add(txtFieldMinService);
        
        maxServTime = new JLabel ("Max service time");
        panou.add(maxServTime);
        panou.add(txtFieldMaxService);

        minArv= new JLabel("Min arriving time");
        panou.add(minArv);
        panou.add(txtFieldMinArriving);
        
        maxArv = new JLabel ("Max arriving time");
        panou.add(maxArv);
        panou.add(txtFieldMaxArriving);

        cashRegCount = new JLabel("Number of cash registers");
        panou.add(cashRegCount);
        panou.add(txtFieldCashRegisterCount);

        totalTime=new JLabel("Total time in seconds");
        panou.add(totalTime);
        panou.add(txtFieldTotalTime);

        start = new JLabel("start");
        panou.add(start);
        panou.add(startBtn);

       frame.add(panou);
       frame.setResizable(false);
       frame.setVisible(true);
       
    
        this.startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                  Long  minServiceTime = Long.parseLong(txtFieldMinService.getText());
                  Long  maxServiceTime = Long.parseLong(txtFieldMaxService.getText());
                  Long  minArrivingTime = Long.parseLong(txtFieldMinArriving.getText());
                  Long  maxArrivingTime = Long.parseLong(txtFieldMaxArriving.getText());
                  Integer totalTime = Integer.parseInt(txtFieldTotalTime.getText());
                  Integer numberOfCashRegisters = Integer.parseInt(txtFieldCashRegisterCount.getText());

                  if (minServiceTime > maxServiceTime
                          || minArrivingTime > maxArrivingTime
                          || totalTime < 0
                          || minServiceTime < 0
                          || minArrivingTime < 0
                          || numberOfCashRegisters < 1
                  ) {
                      throw new Exception("Error!!");
                  }
                  startSimulation(minServiceTime, maxServiceTime, minArrivingTime, maxArrivingTime, totalTime, numberOfCashRegisters);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(getFrame(), ex.getMessage());
                }
            }
        });
    }

    protected Component getFrame() {
		// TODO Auto-generated method stub
		return null;
	}

	private void startSimulation(Long minServiceTime, Long maxServiceTime, Long minArrivingTime, Long maxArrivingTime, Integer totalTime, Integer numberOfCashRegisters) {
        timer = new Timer();
        timer.start();
        for (int i = 0; i < numberOfCashRegisters; i++) {
            cashRegisterList.add(new CashRegister(i, timer, totalTime));
        }

        for(int i = 0; i < numberOfCashRegisters ; i++) {
            Thread thread = new Thread(cashRegisterList.get(i));
            thread.start();
        }

        this.simulationHandler = new SimulationHandler(timer,cashRegisterList,totalTime,minServiceTime,maxServiceTime,minArrivingTime,maxArrivingTime);
        Thread thread = new Thread(simulationHandler);
        thread.start();

        simulationPanel = new SimulationPanel(cashRegisterList);
        frame.setContentPane(simulationPanel);
        frame.invalidate();
        frame.validate();
        frame.repaint();

        long startSimulationTime = timer.getSecondsPassed();

        javax.swing.Timer timerS = new javax.swing.Timer(500, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.invalidate();
                frame.validate();
                frame.repaint();
                simulationPanel.repaint();

                if (startSimulationTime + timer.getSecondsPassed() > totalTime) {
                    thread.stop();
                    JOptionPane.showMessageDialog(null, "Simulation ends");
                    try {
                        ((javax.swing.Timer)e.getSource()).stop();
                        Logger logger = Logger.getInstance();
                        logger.closeFile();

                        File file = new File("simulation.log");
                        Desktop desktop = Desktop.getDesktop();
                        desktop.open(file);
                        stopSimulation();
                    } catch (IOException e1) {
                        System.out.println(e1.getMessage());
                    }

                }
            }
        });
        timerS.start();
    }

    private void stopSimulation() {
        JPanel outputPanel = new JPanel();
        outputPanel.setLayout(new GridLayout(0, 1));
        outputPanel.add(new JLabel("Numarul total de clienti: "  + this.simulationHandler.getTotalCustomers()));
        for(CashRegister cashRegister: this.cashRegisterList)  {
            outputPanel.add(new JLabel("La casa " + cashRegister.getId() + " au fost un numar total de " + cashRegister.getTotalCustomers() + " clienti."));
        }


        outputPanel.add(new JLabel("Timpul mediu de asteptare: "  + this.simulationHandler.getAverageWaitingTime()));
        outputPanel.add(new JLabel("Timpul mediu de servire: "  + this.simulationHandler.getAverageOfServingTime()));

        JTable tableCashRegistersEvolutionHour = new JTable();

        Pair<Integer, Integer> peekHourPair = simulationHandler.getPeekHour();
        Pair<Object[][], Object[]> simulationEvolution = simulationHandler.getSimulationEvolution();

        outputPanel.add(new JLabel("Peek second: " + peekHourPair.getKey() + ", value: " + peekHourPair.getValue()));

        tableCashRegistersEvolutionHour.setModel(new DefaultTableModel(simulationEvolution.getKey(), simulationEvolution.getValue()));

        outputPanel.add(new JScrollPane(tableCashRegistersEvolutionHour));

        frame.setContentPane(outputPanel);
        frame.invalidate();
        frame.validate();
        frame.repaint();
    }

    private class SimulationPanel extends JPanel{
        private List<CashRegister> cashRegisters;

        SimulationPanel(List<CashRegister> cashRegisters) {
            this.cashRegisters = cashRegisters;
        }

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            g.setColor(LightSalmon);
            g.fillRect(0,0, this.getWidth(), this.getHeight());

            int row = 0;
            int col = 0;
            for (CashRegister cashRegister: this.cashRegisters) {
                col = 0;
                row += 50;
                g.setColor(Color.BLUE);
                g.drawOval(row, col, 30, 30);

                List<Client> clientList = cashRegister.getClients();
                g.setColor(Color.RED);
                for (Client client: clientList) {
                    col += 50;
                    g.drawOval(row, col, 25, 25);
                }
            }
        }
    }
}
