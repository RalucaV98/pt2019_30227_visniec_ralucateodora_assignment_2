package casaDeMarcat;

import model.Client;
import simulare.Logger;
import simulare.Timer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CashRegister implements Runnable {

    private int id;
    private Timer timer;
    private long finalTime;
    private List<Client> queue;

    private long timeArrived = 0; 

    private List<Integer> totalClientsPerSecond;
    private javax.swing.Timer hoursTimers;

    private Integer totalCustomers = 0;

    public CashRegister(int id, Timer timer, long finalTime) {
        this.timer = timer;
        this.id = id;
        this.finalTime = finalTime;
        this.queue = new ArrayList<>();
        this.totalClientsPerSecond = new ArrayList<>();

        /**
         * Setez timer-ul pentru inserarea in lista a numarului de clienti activi la casa la o durata de 1 secunda
         */
        this.hoursTimers = new javax.swing.Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalClientsPerSecond.add(queue.size());
            }
        });

    }

    /**
     * Metoda de run a thread-ului
     * Acesta este pus in sleep atatea secunde cat clientul din capul cozii are timpul de servire
     * Se scrie in fisier folosindu-ma de clasa Logger: casa, timpul la care a pleacat si clientul
     */
    public void run() {
        this.hoursTimers.start();
        while (timer.getSecondsPassed() < finalTime) {
            try {
                if (this.queue.isEmpty()) {
                    Thread.sleep(1000);//astept minim o secunda pana vine primul client
                } else {
                    Client client = this.queue.get(0);
                    timeArrived = timer.getSecondsPassed();
                    Thread.sleep(client.getServiceTime() * 1000);
                    this.queue.remove(0);
                    client.setFinalTime(timer.getSecondsPassed());

                    Logger logger = Logger.getInstance();
                    String message = "Coada " + this.id + ":pleaca la secunda " + timer.getSecondsPassed() + " " + client;
                    logger.write(message);
                }
            } catch (InterruptedException | IOException e) {
                System.out.println(e.getMessage());
            }
        }
        this.hoursTimers.stop();
    }

    /**
     * Adauga un client la coada
     * @param client
     */
    public synchronized void addClient(Client client) {
        this.totalCustomers++;
        this.queue.add(client);
        notifyAll();//atentionez celelalte threaduri de modificari
    }

    /**
     * Calculeaza timpul de asteptare a urmatorului client in functie de clientii ce sunt deja la coada
     * @return
     */
    public long getTotalWaitingTime() {
        int time = 0;

        if (this.queue.size() > 0) {
            time += (timeArrived + this.queue.get(0).getServiceTime()) - timer.getSecondsPassed();
        }

        for (int i = 1; i < this.queue.size(); i++) {
            time += this.queue.get(i).getServiceTime();
        }

        return (time >= 0) ? time : 0;
    }

    /**
     * Returneza o lista cu clientii ce sunt la casa
     * @return
     */
    public List<Client> getClients() {
        return this.queue;
    }

    /**
     * returneaza o lista cu numarul de clienti pentru fiecare secunda din timpul simularii
     * @return
     */
    public List<Integer> getCashRegisterEvolution() {
        return this.totalClientsPerSecond;
    }

    public int getId() {
        return this.id;
    }

    public Integer getTotalCustomers() {
        return this.totalCustomers;
    }
}
