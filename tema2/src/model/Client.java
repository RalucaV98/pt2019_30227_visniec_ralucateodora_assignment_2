package model;

public class Client {
	private long id;
	private long arrivalTime;
	private long serviceTime;
	private long finalTime;
	
	public Client() {
		
	}
	
	public Client(long id, long arrivalTime, long serviceTime) {
		this.arrivalTime=arrivalTime;
		this.serviceTime=serviceTime;
		this.id=id;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(long arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public long getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(long serviceTime) {
		this.serviceTime = serviceTime;
	}

	public long getFinalTime() {
		return finalTime;
	}

	public void setFinalTime(long finalTime) {
		this.finalTime = finalTime;
	}
	
	
	public String toString() {
		return "Client{" + "id=" + id + ", arrivalTime=" + arrivalTime +
				", serviceTime=" + serviceTime + ", finalTime=" + finalTime + "}";
	}
}
